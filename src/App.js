import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";
import Header from "./components/Header/Header";
import Todo from "./containers/Todo/Todo";
import Counter from "./containers/Counter/Counter";
import './App.css';

import axios from 'axios';
axios.defaults.baseURL = 'https://kurlov-hw64-64d41.firebaseio.com/';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <div className="App-body">
          <Switch>
            <Route path="/" exact component={Todo} />
            <Route path="/todo" component={Todo}/>
            <Route path="/counter" component={Counter}/>
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
