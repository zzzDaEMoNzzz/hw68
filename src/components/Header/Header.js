import React from 'react';
import './Header.css';
import {NavLink, withRouter} from "react-router-dom";

const Header = props => {
  const isTodoList = () => {
    const url = props.location.pathname;
    return url === '/' || url.includes('todo');
  };

  return (
    <header className="Header">
      <nav>
        <NavLink to="/todo" isActive={isTodoList}>Todo</NavLink>
        <NavLink to="/counter">Counter</NavLink>
      </nav>
    </header>
  );
};

export default withRouter(Header);