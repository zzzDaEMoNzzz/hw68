import React, {Component, Fragment} from 'react';
import Preloader from "../components/Preloader/Preloader";

const withLoaderHandler = (WrappedComponent, axios) => {
  return class withLoaderHandlerHOC extends Component {
    constructor(props) {
      super(props);

      this.state = {
        loading: true
      };

      this.state.interceptorRequestID = axios.interceptors.request.use(req => {
        this.setState({loading: true});
        return req;
      });

      this.state.interceptorResponseID = axios.interceptors.response.use(res => {
        this.setState({loading: false});
        return res;
      });
    }

    componentWillUnmount() {
      axios.interceptors.response.eject(this.state.interceptorResponseID);
      axios.interceptors.request.eject(this.state.interceptorRequestID);
    }

    render() {
      return (
        <Fragment>
          <Preloader show={this.state.loading}/>
          <WrappedComponent {...this.props}/>
        </Fragment>
      );
    }
  };
};

export default withLoaderHandler;
