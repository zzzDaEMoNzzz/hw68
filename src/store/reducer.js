import {
  ADD_COUNTER,
  ADD_TASK,
  DECREMENT_COUNTER, FETCH_COUNTER_ERROR,
  FETCH_COUNTER_REQUEST, FETCH_COUNTER_SUCCESS,
  FETCH_TASKS,
  INCREMENT_COUNTER,
  SUBTRACT_COUNTER,
  TODO_INPUT_ON_CHANGE
} from "./actions";

const initialState = {
  tasks: [],
  toDoInput: '',
  counter: 0,
  loading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK:
      return {
        ...state,
        toDoInput: ''
      };
    case FETCH_TASKS:
      return {
        ...state,
        tasks: action.tasks
      };
    case TODO_INPUT_ON_CHANGE:
      return {
        ...state,
        toDoInput: action.value
      };
    case INCREMENT_COUNTER:
      return {
        ...state,
        counter: state.counter + 1
      };
    case DECREMENT_COUNTER:
      return {
        ...state,
        counter: state.counter - 1
      };
    case ADD_COUNTER:
      return {
        ...state,
        counter: state.counter + action.amount
      };
    case SUBTRACT_COUNTER:
      return {
        ...state,
        counter: state.counter - action.amount
      };
    case FETCH_COUNTER_REQUEST:
      return {
        ...state,
        loading: true
      };
    case FETCH_COUNTER_SUCCESS:
      return {
        ...state,
        counter: action.counter,
        loading: false
      };
    case FETCH_COUNTER_ERROR:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
};

export default reducer;