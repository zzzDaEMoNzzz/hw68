import axios from "axios";

export const ADD_TASK = 'ADD_TASK';
export const TODO_INPUT_ON_CHANGE = 'TODO_INPUT_ON_CHANGE';
export const FETCH_TASKS = 'FETCH_TASKS';

export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
export const DECREMENT_COUNTER = 'DECREMENT_COUNTER';
export const ADD_COUNTER = 'ADD_COUNTER';
export const SUBTRACT_COUNTER = 'SUBTRACT_COUNTER';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';

export const todoInputOnChangeHandler = event => {
  return {type: TODO_INPUT_ON_CHANGE, value: event.target.value};
};

export const addTask = event => {
  event.preventDefault();

  return (dispatch, getState) => {
    const task = getState().toDoInput;

    axios.post('todo.json', {task}).then(() => {
      dispatch({type: ADD_TASK});
      dispatch(fetchTasks());
    });
  };
};

export const deleteTask = id => {
  return dispatch => {
    axios.delete(`todo/${id}.json`).then(() => {
      dispatch(fetchTasks());
    });
  };
};

export const fetchTasks = () => {
  return dispatch => {
    axios.get('todo.json').then(response => {

      let tasks = [];

      if (response.data && Object.keys(response.data).length > 0) {
        const tasksIDs = Object.keys(response.data);
        tasks = tasksIDs.reduce((array, id) => {
          array.push({id: id, ...response.data[id]});
          return array;
        }, []);
      }

      dispatch({type: FETCH_TASKS, tasks});
    });
  };
};

export const incrementCounter = () => {
  return dispatch => {
    dispatch({type: INCREMENT_COUNTER});
    dispatch(saveCounter())
  };
};

export const decrementCounter = () => {
  return dispatch => {
    dispatch({type: DECREMENT_COUNTER});
    dispatch(saveCounter())
  };
};

export const addCounter = amount => {
  return dispatch => {
    dispatch({type: ADD_COUNTER, amount});
    dispatch(saveCounter())
  };
};

export const subtractCounter = amount => {
  return dispatch => {
    dispatch({type: SUBTRACT_COUNTER, amount});
    dispatch(saveCounter())
  };
};

export const fetchCounterRequest = () => {
  return {type: FETCH_COUNTER_REQUEST};
};

export const fetchCounterSuccess = counter => {
  return {type: FETCH_COUNTER_SUCCESS, counter};
};

export const fetchCounterError = error => {
  return {type: FETCH_COUNTER_ERROR, error};
};

export const fetchCounter = () => {
  return dispatch => {
    dispatch(fetchCounterRequest());

    axios.get('counter.json').then(response => {
      dispatch(fetchCounterSuccess(response.data));
    }, error => {
      dispatch(fetchCounterError());
    });
  };
};

export const saveCounter = () => {
  return (dispatch, getState) => {
    const counter = getState().counter;

    axios.put('counter.json', counter);
  };
};