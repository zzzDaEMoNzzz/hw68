import React, {Component} from 'react';
import {connect} from "react-redux";
import axios from 'axios';

import TodoAdd from "./TodoAdd/TodoAdd";
import TodoList from "./TodoList/TodoList";
import {addTask, deleteTask, fetchTasks, todoInputOnChangeHandler} from "../../store/actions";
import withLoaderHandler from "../../hoc/withLoader";

class Todo extends Component {
  componentDidMount() {
    this.props.fetchTasks();
  }

  render() {
    return (
      <div>
        <TodoAdd
          inputValue={this.props.inputValue}
          inputOnChange={this.props.onChangeInputValue}
          formOnSubmit={this.props.addTask}
        />
        <h3>Todo list:</h3>
        <TodoList
          tasks={[...this.props.tasks].reverse()}
          deleteTask={this.props.deleteTask}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tasks: state.tasks,
    inputValue: state.toDoInput
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchTasks: () => dispatch(fetchTasks()),
    deleteTask: id => dispatch(deleteTask(id)),
    onChangeInputValue: event => dispatch(todoInputOnChangeHandler(event)),
    addTask: event => dispatch(addTask(event))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withLoaderHandler(Todo, axios));